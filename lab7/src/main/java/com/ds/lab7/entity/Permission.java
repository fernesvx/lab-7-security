package com.ds.lab7.entity;

import com.ds.lab7.Role;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table
public class Permission {
    @Id
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(unique = true)
    private Role role;
}
