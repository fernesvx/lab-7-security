package com.ds.lab7.repository;

import com.ds.lab7.Role;
import com.ds.lab7.entity.Permission;
import org.springframework.data.repository.CrudRepository;


public interface RoleRepository extends CrudRepository<Permission, Long> {
    Permission findPermissionByRole(Role type);
}
