package com.ds.lab7.service;

import com.ds.lab7.Role;
import com.ds.lab7.entity.Permission;
import com.ds.lab7.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private final RoleRepository repository;

    @Autowired
    public RoleService(RoleRepository repository){
        this.repository = repository;
    }

    public Permission findRoleByType(Role role) throws Exception {
        if (!isValidType(role.toString())){
            throw new Exception("Role Properties must be invalid");
        }

        Permission permission = repository.findPermissionByRole(role);

        if(permission == null){
            throw new Exception("Role not found by type");
        }

        return permission;
    }

    private boolean isValidType(String type){
        return !type.isEmpty() && type.length() <= 20;
    }
}
