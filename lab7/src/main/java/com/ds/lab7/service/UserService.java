package com.ds.lab7.service;

import com.ds.lab7.entity.Permission;
import com.ds.lab7.entity.User;
import com.ds.lab7.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final RoleService roleService;
    private final UserRepository repository;
    private final BCryptPasswordEncoder encoder;

    @Autowired
    public UserService(UserRepository repository, RoleService roleService) {
        this.repository = repository;
        this.roleService = roleService;
        encoder = new BCryptPasswordEncoder();
    }

    public User createUser(User user) throws Exception{
        Permission permission = roleService.findRoleByType(user.getPermission().getRole());
        user.setPermission(permission);

        user.setPassword(encoder.encode(user.getPassword()));
        return repository.save(user);
    }

}
